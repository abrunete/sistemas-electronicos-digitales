Ejemplos de ADC. Sensor analógico conectado al PA1.

abg_adc_polling: petición por polling, em modo continuo. Sin modo continuo descomentar Start y Stop. 

abg_adc_int: igual que antes pero con interrupción

abg_adc_int_scan: ADC con interrupción, modo scan con un sensor conectado a PA1 y el sensor de temperatura interno. Se añade TIM2 para verificar tiempo de conversión.

abg_adc_dma: mediate DMA, se utiliza además del PA1 el sensor de temperatura interno. Se calcula la temperatura en grados centígrados a partir de la medida del ADC.