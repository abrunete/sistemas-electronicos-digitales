Ejemplos de comunicaciones serie

abg_uart_polling: ejemplo sencillo de comunicaciones por el UART2 con un PC

abg_uart_int: ejemplo de uso de interrupciones, UART2 y ringbuffer

abg_spi_acc: ejemplo de comunicación SPI con el giroscopo l3gd20 integrado en la placa Discovery

abg_spi_acc_2: ejemplo de comunicación SPI con el giroscopo l3gd20 integrado en la placa Discovery. Se añade cálculo de la rotación a partir de los valores de las velocidades angulares medidos. Integrador + offset + noise.
