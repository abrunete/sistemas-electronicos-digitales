Ejemplos de temporizadores

abg_timers_TIM_1: ejemplo simple de un temporizador b�sico (sin entradas ni salidas), se utiliza para cambiar el estado de un LED.

abg_timers_inputcapture_boton: ejemplo de temporizador avanzado con Input Capture. Captura el momento en el que se pulsa el PA0.

abg_timers_outputcmp_1: ejemplo de Output Compare. Se encienden y apagan los LEDS (12-15) en orden en disitnitos intervalos.

abg_timers_PWM_LEDBuzzer: ejmplo de uso de PWM para regular la intensidad del LED PD12 y regular la intensidad de un "zumbador" conectado a la entrada PE9.

abg_timers_servo: ejemplo de uso de un servomotor conectad al PE9.

abg_timers_buzzer: ejemplo de generaci�n de sonidos en un zumbador (buzzer) conectado al PA1.

abg_timer_IC_dma: ejemplo de uso de IC con DMA (circular). Cada vez que se pulsa el PA0 se guarda en un vector de 10 posiciones el valor del contador del timer.
